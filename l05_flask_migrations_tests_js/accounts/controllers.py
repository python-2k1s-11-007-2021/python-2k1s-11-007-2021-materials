from flask import request, redirect, url_for, flash, render_template, Blueprint, abort
from flask_login import login_user, login_required, logout_user, current_user

from accounts.services import find_by_username_and_password, IncorrectPasswordError, NotFoundError, find_user_by_id

accounts_blueprint = Blueprint('accounts', __name__, template_folder='templates')


@accounts_blueprint.route('/login', methods=('GET', 'POST'))
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        remember_me = len(request.form.getlist('remember_me')) > 0
        try:
            user = find_by_username_and_password(username, password)
        except Exception as e:
            flash(str(e) or 'Unknown error')
            return redirect(url_for('accounts.login'))
        else:
            login_user(user, remember=remember_me)
            return redirect(url_for('company_management.main_page'))
    return render_template('accounts/login.html')


@accounts_blueprint.route("/logout", methods=["GET"])
@login_required
def logout():
    logout_user()
    return redirect(url_for('company_management.main_page'))


@accounts_blueprint.route("/user/<int:user_id>", methods=["GET"])
@login_required
def user_info(user_id: int):
    try:
        user = find_user_by_id(user_id)
        return render_template('accounts/user_info.html', user=user)
    except NotFoundError:
        abort(404)


@accounts_blueprint.route("/user", methods=["GET"])
@login_required
def current_user_info():
    return render_template('accounts/user_info.html', user=current_user)
