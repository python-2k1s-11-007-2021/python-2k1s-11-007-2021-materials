import pytest
from flask import url_for
from werkzeug.security import generate_password_hash

import app as app_module
from accounts.models import User


@pytest.fixture
def app():
    app_module.app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://'
    app_module.app.config['WTF_CSRF_ENABLED'] = False
    with app_module.app.app_context():
        app_module.db.create_all()
    yield app_module.app
    with app_module.app.app_context():
        app_module.db.session.remove()
        app_module.db.drop_all()


@pytest.fixture
def logged_in_client(client):
    app_module.db.session.add(User(username='Rustem', hashed_password=generate_password_hash('123456')))
    app_module.db.session.commit()
    client.post(url_for('accounts.login'), data=dict(username='Rustem', password='123456'))
    yield client
    client.get(url_for('accounts.logout'))
