import multiprocessing
import os
import socket

# No inspection is added because these imports register command functions
# noinspection PyUnresolvedReferences
from commands import get_value, set_value, bye
from dictionaries import commands_dict, shared_dict
# No inspection is added to show long import work
# noinspection PyUnresolvedReferences
import long_import


def handle_command(cmd, *args):
    try:
        command_func = commands_dict.get(cmd)
        if command_func is None:
            raise ValueError('Unknown command')
        result = command_func(*args)
    except (TypeError, ValueError, AssertionError) as e:
        result = str(e)
    except StopIteration:
        result = ''
    return result


def handle_client(conn, addr, semaphore):
    with semaphore:
        print(f'Using process {multiprocessing.current_process()} for client: {addr}')
        read_file = conn.makefile(mode='r', encoding='utf-8')
        write_file = conn.makefile(mode='w', encoding='utf-8')
        write_file.write('Hi client\n')
        write_file.flush()
        cmd = read_file.readline().strip()
        while cmd:
            result = handle_command(*cmd.split())
            if result == '':
                break
            write_file.write(str(result) + '\n')
            write_file.flush()
            cmd = read_file.readline().strip()
        conn.close()


def main():
    # multiprocessing.set_start_method('fork')
    # multiprocessing.set_start_method('forkserver')
    # multiprocessing.set_forkserver_preload(['long_import'])
    host = '0.0.0.0'
    port = 8080
    semaphore = multiprocessing.Semaphore(3)
    print(f'Started process with PID={os.getpid()}')

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        # solution for "[Error 89] Address already in use". Use before bind()
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind((host, port))
        s.listen(5)
        try:
            while True:
                with semaphore:
                    conn, addr = s.accept()
                    shared_dict['accepted'] = shared_dict.get('accepted', 0) + 1
                    t = multiprocessing.Process(target=handle_client, args=(conn, addr, semaphore), daemon=True)
                    t.start()
        except (KeyboardInterrupt, SystemExit):
            print('\nReceived keyboard interrupt, quitting threads.\n')


if __name__ == '__main__':
    main()
